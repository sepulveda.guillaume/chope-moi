import React, { useState, useEffect } from "react";
import AuthentificationNavigation from "./navigation/AuthentificationNavigation";
import AuthenticatedNavigation from "./navigation/AuthenticatedNavigation";
import { auth } from './config/firebase';
import { LogBox } from 'react-native';
import 'react-native-reanimated';

LogBox.ignoreLogs(['Remote debugger']);
LogBox.ignoreLogs(['Setting a timer']);
LogBox.ignoreLogs(['AsyncStorage has been extracted']);
LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
LogBox.ignoreLogs(['If you want to use Reanimated 2 then go through our installation steps']);
LogBox.ignoreLogs(['Warning: Can\'t perform a React state update on an unmounted component.']);
LogBox.ignoreLogs(['Cannot read properties of undefined']);
LogBox.ignoreLogs(['VirtualizedLists should never be nested']);
LogBox.ignoreLogs(['FirebaseError: Firebase: Error']);
LogBox.ignoreLogs(['_firestore.doc.data is not a function']);

function App() {

  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  function onAuthStateChanged(user) {
    setUser(user);
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = auth.onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  if (initializing) return null;

  if (!user || user.emailVerified == false) {
    return (
      <AuthentificationNavigation />
    )
  }
  else {
    return (
      <AuthenticatedNavigation />
    )
  }
}

export default App;
