import 'dotenv/config';

export default {
  expo: {
    name: 'chope-moi',
    slug: 'chope-moi',
    version: '1.0.0',
    orientation: 'portrait',
    icon: './assets/expo/icon.png',
    splash: {
      // image: './assets/splash.png',
      resizeMode: 'contain',
      backgroundColor: '#000000'
    },
    updates: {
      fallbackToCacheTimeout: 0
    },
    assetBundlePatterns: ['**/*'],
    ios: {
      supportsTablet: true
    },
    android: {
      adaptiveIcon: {
        foregroundImage: './assets/expo/adaptive-icon.png',
        backgroundColor: '#FFFFFF'
      }
    },
    web: {
      favicon: './assets/expo/favicon.png'
    },
  }
};