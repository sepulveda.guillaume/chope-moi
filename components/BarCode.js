import React from "react";
import { StyleSheet, View } from "react-native";
import { useNavigation } from '@react-navigation/native';
import { Ionicons } from '@expo/vector-icons';

export default function BarCode() {
    const navigation = useNavigation();

    return (
        <View style={styles.container} >
            <Ionicons
                name="barcode-outline"
                size={40}
                color="white"
                onPress={() => navigation.navigate("Scan")}
            />
        </View >
    )
}

const styles = StyleSheet.create({
    container: {
        width: 70,
        height: 70,
        borderRadius: 35,
        backgroundColor: '#3a8dff',
        alignItems: 'center',
        justifyContent: 'center',
        position: "absolute",
        bottom: 15,
        right: 15,
        shadowColor: "#000",
        elevation: 5,
    },
});