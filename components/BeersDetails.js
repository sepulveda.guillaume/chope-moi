import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Dimensions } from 'react-native';

export default function BeersDetailsScreen(props) {

    return (
        <TouchableOpacity onPress={() => props.displayDetailsForBeer(props.item.code)} style={styles.container} >
            <View style={styles.allProducts}>
                {props.item.image_url ? <Image source={{ uri: props.item.image_url }} style={styles.image} /> : null}
                <View style={styles.noteProduct}>
                    <Text style={styles.productName}>{props.item.product_name}</Text>
                    <Text style={styles.productGenericName}>{props.item.generic_name} </Text>
                    <View>
                        <Text style={styles.resultsProduct}>{props.item.categories}</Text>
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    )
}

let width = Dimensions.get('window').width; //full width

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

    image: {
        height: 150,
        width: width * 0.15,
        borderRadius: 2,
        resizeMode: 'contain',
        marginRight: 10
    },

    allProducts: {
        display: "flex",
        flexDirection: "row",
        borderRadius: 2,
        marginBottom: 10,
        paddingVertical: 10,
        paddingHorizontal: 10,
        backgroundColor: 'white'
    },

    noteProduct: {
        width: width * 0.7
    },

    productName: {
        fontWeight: "bold",
        fontSize: 18,
        color: "#272727",
        marginTop: 10
    },

    productGenericName: {
        marginTop: 10,
        color: "#3a8dff",
        fontWeight: "bold"
    },

    resultsProduct: {
        display: "flex",
        flexDirection: "row",
        color: "#808080",
        marginTop: 10,
        paddingRight: 10
    },

    lineStyle: {
        borderWidth: 0.5,
        borderColor: '#808080',
        margin: 10,
    }
});