import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Moment from 'moment';
import { Rating } from 'react-native-ratings';

export default function BeersDetailsScreen(props) {

    return (
        <>
            <View style={styles.header}>
                <Text style={styles.email}>{props.item.email}</Text>
                <Text style={styles.date}>{Moment(props.item.date.toDate()).locale('fr').format('LL')}</Text>
            </View>
            <Rating
                type='heart'
                startingValue={props.item.note}
                style={{ paddingVertical: 10 }}
                ratingCount={5}
                imageSize={30}
                readonly
            />
            <Text>{props.item.comment}</Text>
        </>
    )
}

const styles = StyleSheet.create({
    header: {
        flex: 1,
    },

    email: {
        color: '#3a8dff',
        fontWeight: 'bold',
        alignSelf: "flex-start",
        marginBottom: 5
    },

    date: {
        alignSelf: "flex-end",
        color: "#808080",
        marginBottom: 5
    }
});