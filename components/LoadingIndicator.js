import React from "react";
import { View, StyleSheet, ActivityIndicator } from "react-native";

export default function LoadingIndicator() {

    return (
        <View style={styles.container}>
            <ActivityIndicator size="large" color="#3a8dff" />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10,
    },
});