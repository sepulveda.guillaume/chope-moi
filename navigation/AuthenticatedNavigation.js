import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from "../screens/HomeScreen";
import SearchScreen from "../screens/SearchScreen";
import FavoriteScreen from "../screens/FavoriteScreen";
import ProfilScreen from "../screens/ProfilScreen";
import ScanScreen from "../screens/ScanScreen";
import BeerDetailsScreen from "../screens/BeerDetailsScreen"
import { Ionicons } from "@expo/vector-icons";
import 'react-native-reanimated';

const Tab = createBottomTabNavigator();
const HomeStack = createNativeStackNavigator();
const SearchStack = createNativeStackNavigator();
const FavoriteStack = createNativeStackNavigator();

const activeTintLabelColor = '#3a8dff';
const inactiveTintLabelColor = '#808080';

function HomeStackNavigation() {
    return (
        <HomeStack.Navigator>
            <HomeStack.Screen
                name="Accueil"
                component={HomeScreen}
                options={{
                    headerShown: false
                }}
            />
            <HomeStack.Screen
                name="Scan"
                component={ScanScreen}
                options={{
                    headerShown: false
                }}
            />
            <HomeStack.Screen
                name="Détails"
                component={BeerDetailsScreen}
            />
        </HomeStack.Navigator>
    );
};

function SearchStackNavigation() {
    return (
        <SearchStack.Navigator>
            <SearchStack.Screen
                name="Search"
                component={SearchScreen}
                options={{
                    headerShown: false,
                    title: 'Rechercher',
                }}
            />
            <SearchStack.Screen
                name="Détails"
                component={BeerDetailsScreen}
                tabBarVisible={false}
            />
        </SearchStack.Navigator>
    );
};

function FavoriteStackNavigation() {
    return (
        <FavoriteStack.Navigator>
            <FavoriteStack.Screen
                name="Favorite"
                component={FavoriteScreen}
                options={{
                    headerShown: false,
                }}
            />
            <FavoriteStack.Screen
                name="Détails"
                component={BeerDetailsScreen}
            />
        </FavoriteStack.Navigator>

    );
};

export default function AuthenticatedNavigation({ navigation, route }) {

    return (
        <NavigationContainer>
            <Tab.Navigator
                initialRouteName="Derniers scans"
                screenOptions={{
                    tabBarHideOnKeyboard: "true",
                    headerShown: false,
                }}

            >
                <Tab.Screen
                    name="Derniers scans"
                    component={HomeStackNavigation}
                    options={{
                        tabBarIcon: ({ focused }) => (
                            <Ionicons name="beer" color={focused ? activeTintLabelColor : inactiveTintLabelColor} size={24} />
                        ),
                        tabBarLabel: 'Scans',
                        tabBarVisible: false,
                    }}
                />
                <Tab.Screen
                    name="Rechercher"
                    component={SearchStackNavigation}
                    options={{
                        tabBarIcon: ({ focused }) => (
                            <Ionicons name="search" color={focused ? activeTintLabelColor : inactiveTintLabelColor} size={24} />
                        ),
                        tabBarLabel: 'Recherche',
                    }}
                />
                <Tab.Screen
                    name="Favoris"
                    component={FavoriteStackNavigation}
                    options={{
                        tabBarIcon: ({ focused }) => (
                            <Ionicons name="heart" color={focused ? activeTintLabelColor : inactiveTintLabelColor} size={24} />
                        ),
                        tabBarLabel: 'Favoris',
                    }}
                />
                <Tab.Screen
                    name="Profil"
                    component={ProfilScreen}
                    options={{
                        tabBarIcon: ({ focused }) => (
                            <Ionicons name="person-circle-outline" color={focused ? activeTintLabelColor : inactiveTintLabelColor} size={24} />
                        ),
                        tabBarLabel: 'Profil',
                    }}
                />
            </Tab.Navigator>
        </NavigationContainer >
    );
}

