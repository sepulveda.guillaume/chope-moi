import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import LoginScreen from "../screens/LoginScreen";
import RegisterScreen from "../screens/RegisterScreen";
import ForgotPasswordScreen from "../screens/ForgotPasswordScreen";

const AuthentificationStack = createNativeStackNavigator();

export default function AuthentificationNavigation() {

    return (
        <NavigationContainer>
            <AuthentificationStack.Navigator
                screenOptions={{
                    headerShown: false,
                    headerTintColor: 'black'
                }}>
                <AuthentificationStack.Screen
                    name="Connexion"
                    component={LoginScreen}
                    options={{
                        stackBarLabel: () => { return null },
                    }}
                />
                <AuthentificationStack.Screen
                    name="Inscription"
                    component={RegisterScreen}
                />
                <AuthentificationStack.Screen
                    name="Mot de passe oublié"
                    component={ForgotPasswordScreen}
                />
            </AuthentificationStack.Navigator>
        </NavigationContainer>
    );
};