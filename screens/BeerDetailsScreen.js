import React, { useState, useEffect } from "react";
import {
    Text,
    View,
    StyleSheet,
    Image,
    Dimensions,
    ScrollView,
} from "react-native";
import { auth, db } from "../config/firebase";
import {
    collection,
    doc,
    getDocs,
    getDoc,
    query,
    where,
    updateDoc,
    arrayUnion,
    arrayRemove,
    onSnapshot,
    orderBy,
} from "firebase/firestore";
import { Ionicons } from "@expo/vector-icons";
import Comments from "../components/Comments";
import { Formik } from "formik";
import LoadingIndicator from "../components/LoadingIndicator";
import { List } from "react-native-paper";
import TextInput from '../components/TextInput';
import { Button } from 'react-native-elements';
import { CommentsValidationSchema } from '../yup/CommentsValidationSchema';
import { Rating } from 'react-native-ratings';
import { isValidFormat } from "firebase/node_modules/@firebase/util";

export default function BeerDetailsScreen(props) {
    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState("");
    const [favorite, setFavorite] = useState("");
    const [ratingCount, setRatingCount] = useState(0);
    const [avgNote, setAvgNote] = useState(null)

    const user = auth.currentUser;

    async function addComment(values, { resetForm }) {
        const user = auth.currentUser;
        const q = query(
            collection(db, "beers"),
            where("code", "==", props.route.params.code)
        );
        const querySnapshot = await getDocs(q);

        const newComment = {
            email: user.email,
            date: new Date(),
            note: ratingCount,
            comment: values.comment,
        };

        let findId = [];

        querySnapshot.forEach((doc) => {
            findId.push(doc.id);
        });

        const beerDoc = doc(db, "beers", findId[0]);

        await updateDoc(beerDoc, {
            comments: arrayUnion(newComment),
        });

        resetForm({ values: "" });
        setRatingCount(0);
    }

    async function getFavoriteBeerUser() {
        const userDoc = doc(db, "users", user.uid);
        const docSnap = await getDoc(userDoc);

        let beers = docSnap.data().beers_favorite;
        let arrayBeers = [];
        beers.forEach((beer) => {
            arrayBeers.push(beer.code);
        });

        if (arrayBeers.includes(props.route.params.code)) {
            setFavorite(true);
        } else {
            setFavorite(false);
        }
    }

    async function getBeerDetails(code) {
        const q = query(collection(db, "beers"), where("code", "==", code));
        const unsubscribe = onSnapshot(q, (querySnapshot) => {
            let sumArray = [];
            let sumNote = 0;
            let count = 0;

            querySnapshot.forEach((doc) => {
                // doc.data() is never undefined for query doc snapshots
                setData(doc.data());

                if (doc.data().comments !== undefined) {
                    sumArray.push(doc.data().comments)

                }
            });

            if (sumArray.length > 0) {
                sumArray[0].forEach((item => {
                    sumNote += parseFloat(item.note);
                    count++
                }))

                setAvgNote(sumNote / count)
            }

            setIsLoading(false);
        });
    }

    async function updateFavorite() {
        const userDoc = doc(db, "users", user.uid);
        const docSnap = await getDoc(userDoc);
        const beerToAdd = {
            code: props.route.params.code,
            date: new Date(),
        };

        let beers = docSnap.data().beers_favorite;
        let arrayBeers = [];
        beers.forEach((beer) => {
            arrayBeers.push(beer.code);
        });

        if (arrayBeers.includes(props.route.params.code)) {
            let beerToDelete;

            beers.some((beer) => {
                if (beer.code !== props.route.params.code) return false;
                beerToDelete = beer;
                return true;
            });
            await updateDoc(userDoc, {
                beers_favorite: arrayRemove(beerToDelete),
            });
            setFavorite(false);
        } else {
            await updateDoc(userDoc, {
                beers_favorite: arrayUnion(beerToAdd),
            });
            setFavorite(true);
        }
    }

    useEffect(() => {
        getFavoriteBeerUser();
    }, [data]);

    useEffect(() => {
        getBeerDetails(props.route.params.code);
    }, []);

    return isLoading ? (
        <LoadingIndicator />
    ) : (
        <ScrollView style={styles.container}>
            <View style={styles.mainContainer}>
                {data.image_url ? (
                    <Image style={styles.image} source={{ uri: data.image_url }}></Image>
                ) : null}
                <View style={styles.product}>
                    <View style={styles.header}>
                        {favorite == true ? (
                            <Ionicons
                                name="heart"
                                size={40}
                                color="red"
                                backgroundColor="transparent"
                                style={{ alignSelf: "flex-end" }}
                                onPress={() => updateFavorite()}
                            />
                        ) : (
                            <Ionicons
                                name="heart-outline"
                                size={40}
                                color="black"
                                backgroundColor="transparent"
                                style={{ alignSelf: "flex-end" }}
                                onPress={() => updateFavorite()}
                            />
                        )}
                    </View>
                    <Text style={styles.productName}>{data.product_name}</Text>
                    <Text style={styles.productBrand}>{data.generic_name}</Text>
                    {
                        avgNote ? (
                            <View style={styles.avgNoteContainer}>
                                <Rating
                                    type='heart'
                                    startingValue={avgNote}
                                    ratingCount={5}
                                    imageSize={30}
                                    readonly
                                />
                                <Text style={styles.avgNoteText}>{avgNote.toFixed(1)}/5</Text>
                            </View>
                        ) : (
                            <View style={styles.avgNoteContainer}>
                                <Rating
                                    type='heart'
                                    startingValue={0}
                                    ratingCount={5}
                                    imageSize={30}
                                    readonly
                                />
                                <Text style={styles.noAvgNoteText}>Non noté</Text>
                            </View>
                        )
                    }
                    <View style={styles.productOtherInfos}>
                        <View style={{ marginTop: 15 }}>
                            <Text>{data.alcohol}°</Text>
                            <Text>{data.quantity}</Text>
                            <Text>Pays : {data.countries}</Text>
                        </View>
                    </View>
                    <Text style={styles.productCategories}>{data.categories}</Text>
                </View>
            </View>
            <List.Accordion
                title="Ingrédients"
                left={(props) => <List.Icon {...props} icon="barley" />}
                theme={{
                    colors: { primary: "#3a8dff" }
                }}
            >
                <List.Item
                    left={() => (
                        <View>
                            {data.ingredients ?
                                <Text style={styles.productIngredients}>{data.ingredients}</Text>
                                :
                                <Text style={styles.productIngredients}>Liste des ingrédients non disponible.</Text>
                            }
                        </View>
                    )}
                />
            </List.Accordion>
            <View style={styles.listSeparator}></View>
            <List.Accordion
                title="Commentaires"
                left={(props) => <List.Icon {...props} icon="message-text-outline" />}
                theme={{
                    colors: { primary: "#3a8dff" },
                }}
            >
                <List.Item
                    left={() => (
                        <View>
                            {data.comments ? data.comments.map((item) => (
                                <View style={styles.commentContainer} key={item.date}>
                                    <Comments item={item} />
                                </View>
                            ))
                                :
                                <Text style={styles.noCommentContainer}>Le produit n'a reçu aucun commentaire pour le moment.</Text>
                            }
                        </View>
                    )}
                />
                <View style={styles.addCommentContainer}>
                    <Text style={styles.title}>
                        Ajouter un commentaire
                    </Text>
                    <Rating
                        type='heart'
                        startingValue={ratingCount}
                        ratingCount={5}
                        imageSize={30}
                        style={{ paddingVertical: 10 }}
                        onFinishRating={rating => setRatingCount(rating)}
                    />
                    <Formik
                        validationSchema={CommentsValidationSchema}
                        initialValues={{ comment: "" }}
                        onSubmit={(values, { resetForm }) =>
                            addComment(values, { resetForm })
                        }
                    >
                        {({
                            handleChange,
                            handleBlur,
                            handleSubmit,
                            errors,
                            isValid,
                            values,
                            touched,
                        }) => (
                            <>
                                <View style={styles.viewInput}>
                                    <TextInput
                                        name="comment"
                                        placeholder="Écrivez un commentaire..."
                                        multiline={true}
                                        numberOfLines={4}
                                        onChangeText={handleChange("comment")}
                                        onBlur={handleBlur("comment")}
                                        value={values.comment}
                                    />
                                </View>
                                {errors.comment && touched.comment ? (
                                    <Text style={{ fontSize: 10, color: "red" }}>
                                        {errors.comment}
                                    </Text>
                                ) : null}
                                <Button
                                    onPress={handleSubmit}
                                    containerStyle={styles.button}
                                    title="Valider"
                                    disabled={!isValid} />
                            </>
                        )}
                    </Formik>
                </View>
            </List.Accordion>
        </ScrollView>
    );
}

let width = Dimensions.get("window").width; //full width

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
    },

    mainContainer: {
        flex: 1,
        display: "flex",
        flexDirection: "row",
        marginVertical: 10,
        marginHorizontal: 10,
        alignItems: "center",
        backgroundColor: "white",
        padding: 10,
    },

    image: {
        height: 300,
        width: width * 0.25,
        borderRadius: 2,
        resizeMode: "contain",
        marginRight: 10,
    },

    product: {
        flex: 1,
    },

    productName: {
        fontWeight: "bold",
        fontSize: 28,
        color: "black",
    },

    productBrand: {
        marginTop: 10,
        color: "#3a8dff",
    },

    productOtherInfos: {
        marginVertical: 10,
    },

    productCategories: {
        marginTop: 10,
        color: "#808080",
    },

    productIngredients: {
        paddingLeft: 10,
        color: "#808080",
    },

    listSeparator: {
        height: 10
    },

    commentContainer: {
        borderWidth: 1,
        borderRadius: 2,
        borderColor: "#808080",
        margin: 10,
        backgroundColor: "white",
        width: width * 0.9,
        padding: 10
    },

    noCommentContainer: {
        paddingLeft: 10,
        paddingBottom: 20,
        color: "#808080",
    },

    addCommentContainer: {
        flex: 1,
        backgroundColor: "white",
        width: width,
        marginTop: 10,
        paddingLeft: 0,
        paddingTop: 10,
        paddingBottom: 25,
        alignItems: "center",
    },

    title: {
        color: '#223e4b',
        fontSize: 18,
        marginVertical: 16,
        textDecorationLine: "underline",
    },

    viewInput: {
        paddingHorizontal: 32,
        marginVertical: 16,
        width: '100%'
    },

    button: {
        height: 44,
        width: 250,
        justifyContent: "center",
        marginTop: 10
    },

    avgNoteContainer: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        marginTop: 20
    },

    avgNoteText: {
        fontWeight: "bold",
        fontSize: 16,
        paddingLeft: 10
    },

    noAvgNoteText: {
        fontWeight: "bold",
        fontSize: 16,
        paddingLeft: 5
    }
});
