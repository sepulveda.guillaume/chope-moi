import React, { useState, useEffect } from "react";
import {
    Text,
    StyleSheet,
    SafeAreaView,
    FlatList,
} from "react-native";
import BeersDetails from "../components/BeersDetails";
import BarCode from "../components/BarCode";
import { auth, db } from "../config/firebase";
import { collection, doc, query, where, onSnapshot } from "firebase/firestore";
import StatusBar from "../components/StatusBar";
import LoadingIndicator from '../components/LoadingIndicator';

export default function FavoriteScreen(props) {

    const user = auth.currentUser;

    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState([]);
    const [message, setMessage] = useState('');

    async function fetchData() {
        const unsub = onSnapshot(doc(db, "users", user.uid), (doc) => {
            let beers = doc.data().beers_favorite;
            let arrayBeers = [];

            beers.forEach((beer) => {
                arrayBeers.push(beer.code)
            })

            if (arrayBeers.length !== 0) {
                setMessage("")
                const q = query(collection(db, "beers"), where("code", 'in', arrayBeers));
                const unsubscribe = onSnapshot(q, (querySnapshot) => {
                    const arrayBeersInfos = [];
                    querySnapshot.forEach((doc) => {
                        arrayBeersInfos.push(doc.data());
                    });
                    setData(arrayBeersInfos);
                    setIsLoading(false);
                });
            } else {
                setMessage("Aucune bière n'a encore été ajoutée dans vos favoris.")
                setIsLoading(false);
            }
        });
    }

    useEffect(() => {
        fetchData();
    }, []);

    function displayDetailsForBeer(code) {
        props.navigation.navigate("Détails", { code: code })
    }

    if (message) {
        return (
            <SafeAreaView style={styles.container} >
                <StatusBar />
                {
                    isLoading ? (
                        <LoadingIndicator />
                    ) : (
                        <Text style={{ fontSize: 18, color: "black", textAlign: "center", width: 300 }}>
                            {message}
                        </Text>
                    )
                }
                <BarCode />
            </SafeAreaView>
        )
    }

    else {
        return (
            <SafeAreaView style={styles.container} >
                <StatusBar />
                {
                    isLoading ? (
                        <LoadingIndicator />
                    ) : (
                        <FlatList
                            data={data}
                            keyExtractor={({ code }) => code}
                            renderItem={({ item }) => <BeersDetails item={item} displayDetailsForBeer={displayDetailsForBeer}
                            />}
                        />
                    )
                }
                <BarCode />
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        marginTop: 10
    },
});
