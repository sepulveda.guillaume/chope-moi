import React, { useState } from 'react';
import { Text, View, StyleSheet, SafeAreaView } from 'react-native';
import { auth } from '../config/firebase';
import { sendPasswordResetEmail } from 'firebase/auth';
import { Formik } from "formik";
import { Button } from 'react-native-elements';
import StatusBar from '../components/StatusBar';
import TextInput from '../components/TextInput';
import { ForgotPasswordValidationSchema } from '../yup/ForgotPasswordValidationSchema';

export default function ForgotPasswordScreen({ navigation }) {

    const [successMessage, setSuccessMessage] = useState('');
    const [errorUserNotFound, seterrorUserNotFound] = useState('');

    function ResetPassword(values, { resetForm }) {
        sendPasswordResetEmail(auth, values.email)
            .then(() => {
                setSuccessMessage('Votre demande de réinitialisation de mot de passe a bien été approuvé. Veuillez consulter le lien transmis par email.')
                resetForm({ values: '' })
                // ..
            })
            .catch((error) => {
                seterrorUserNotFound('Aucun compte n\'a été trouvé avec cette adresse email.')
                const errorCode = error.code;
                const errorMessage = error.message;
                // ..
            });
    }

    return (
        <SafeAreaView style={styles.container} >
            <StatusBar />
            <Formik
                validationSchema={ForgotPasswordValidationSchema}
                initialValues={{ email: "" }}
                onSubmit={(values, { resetForm }) => ResetPassword(values, { resetForm })}
            >
                {({
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    errors,
                    isValid,
                    values,
                    touched,
                }) => (
                    <>
                        <Text style={styles.title}>
                            Mot de passe oublié ?
                        </Text>
                        <View style={styles.viewInput}>
                            <TextInput
                                name="email"
                                placeholder="Email"
                                keyboardType="email-address"
                                icon='mail'
                                autoCapitalize='none'
                                autoCompleteType='email'
                                keyboardAppearance='dark'
                                returnKeyType='next'
                                returnKeyLabel='next'
                                onChangeText={handleChange("email")}
                                onBlur={handleBlur("email")}
                                value={values.email}
                            />
                            {errorUserNotFound ?
                                <Text style={{ fontSize: 10, color: "red" }}>{errorUserNotFound}</Text>
                                : null
                            }
                        </View>
                        <Button
                            onPress={handleSubmit}
                            containerStyle={styles.button}
                            title="Réinitialiser mon mot de passe"
                            disabled={!isValid} />
                        {successMessage ?
                            <Text style={{ fontSize: 10, color: "green", marginTop: 10 }}>{successMessage}</Text>
                            : null
                        }
                    </>
                )}
            </Formik>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
    },

    title: {
        color: '#223e4b',
        fontSize: 20,
        marginBottom: 16
    },

    viewInput: {
        paddingHorizontal: 32,
        marginBottom: 16,
        width: '100%'
    },

    button: {
        height: 44,
        width: 250,
        justifyContent: "center",
        marginTop: 10,
    },
});