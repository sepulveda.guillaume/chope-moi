import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, SafeAreaView, Image } from 'react-native';
import { auth } from '../config/firebase';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { Formik } from "formik";
import { Button } from 'react-native-elements';
import LottieView from 'lottie-react-native';
import StatusBar from '../components/StatusBar';
import TextInput from '../components/TextInput';
import { LoginValidationSchema } from '../yup/LoginValidationSchema';

export default function LoginScreen({ navigation }) {

    const [errorForm, setErrorForm] = useState('');
    const [errorValidation, setErrorValidation] = useState('');
    const [authLoaded, setAuthLoaded] = useState(false);

    function SignInUser(values) {
        signInWithEmailAndPassword(auth, values.email, values.password)
            .then((response) => {
                const user = auth.currentUser;

                if (user.emailVerified == false) {
                    setErrorValidation('Votre compte n\'a pas encore été validé. Veuillez regarder vos emails, y compris vos spams.')
                }
            })
            .catch((error) => {
                values.email = "";
                values.password = "";
                console.log(error);
                setErrorForm('Email et/ou mot de passe incorrect(s)')
            });
    }

    useEffect(() => {
        setTimeout(() => {
            setAuthLoaded(true);
        }, 2000);
    }, []);

    if (authLoaded === false) {
        return (
            <LottieView
                source={require('../assets/lottie/splash.json')}
                autoPlay loop
                style={styles.splash}
            />

        )
    } else {
        return (
            <SafeAreaView style={styles.container} >
                <StatusBar />
                <Image
                    style={styles.logo}
                    source={require('../assets/logo/logo.png')}
                />
                <Text style={styles.textLogo}>Chope-moi</Text>
                <Formik
                    validationSchema={LoginValidationSchema}
                    initialValues={{ email: "", password: "" }}
                    onSubmit={(values) => SignInUser(values)}
                >
                    {({
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        errors,
                        isValid,
                        values,
                        touched,
                    }) => (
                        <>
                            <Text style={styles.title}>
                                Connexion
                            </Text>
                            <View style={styles.viewInput}>
                                <TextInput
                                    name="email"
                                    placeholder="Email"
                                    icon='at-outline'
                                    autoCapitalize='none'
                                    autoCompleteType='email'
                                    keyboardType='email-address'
                                    onChangeText={handleChange("email")}
                                    onBlur={handleBlur("email")}
                                    value={values.email}
                                />
                                {(errors.email && touched.email) ?
                                    <Text style={{ fontSize: 10, color: "red" }}>
                                        {errors.email}
                                    </Text>
                                    : null
                                }
                            </View>
                            <View style={styles.viewInput}>
                                <TextInput
                                    name="password"
                                    placeholder="Mot de passe"
                                    icon='key'
                                    secureTextEntry
                                    autoCompleteType='password'
                                    autoCapitalize='none'
                                    keyboardAppearance='dark'
                                    onChangeText={handleChange("password")}
                                    onBlur={handleBlur("password")}
                                    value={values.password}
                                />
                                {(errors.password && touched.password) ?
                                    <Text style={{ fontSize: 10, color: "red" }}>
                                        {errors.password}
                                    </Text>
                                    : null
                                }
                            </View>
                            <Button
                                type="clear"
                                style={styles.button}
                                onPress={() => navigation.navigate("Mot de passe oublié")}
                                title="Mot de passe oublié ?"
                            />
                            <Button
                                onPress={handleSubmit}
                                containerStyle={styles.button}
                                title="Connexion"
                                disabled={!isValid} />
                            {errorForm ?
                                <Text style={{ fontSize: 10, color: "red" }}>{errorForm}</Text>
                                : null
                            }
                            {errorValidation ?
                                <Text style={{ fontSize: 10, color: "red" }}>{errorValidation}</Text>
                                : null
                            }
                            <Button
                                type="outline"
                                buttonStyle={{
                                    backgroundColor: 'white'
                                }}
                                containerStyle={styles.buttonSignUp}
                                onPress={() => navigation.navigate("Inscription")}
                                title="Inscription"
                            />
                        </>
                    )}
                </Formik>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },

    title: {
        color: '#223e4b',
        fontSize: 20,
        marginBottom: 16
    },

    viewInput: {
        paddingHorizontal: 32,
        marginBottom: 16,
        width: '100%'
    },

    animation: {
        position: "absolute",
        bottom: 245,
        width: 200,
    },

    splash: {
        backgroundColor: 'black'
    },

    logo: {
        width: 150,
        height: 150
    },

    textLogo: {
        fontSize: 30,
        fontVariant: ['small-caps'],
        fontWeight: 'bold',
        includeFontPadding: false,
        marginBottom: 50
    },

    button: {
        height: 44,
        width: 250,
        justifyContent: "center",
        marginTop: 10
    },

    buttonSignUp: {
        height: 44,
        width: 250,
        justifyContent: "center",
        marginTop: 10
    }
});