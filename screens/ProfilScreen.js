import React, { useState } from 'react';
import { Text, View, StyleSheet, SafeAreaView, Alert, Modal } from 'react-native';
import { Button } from 'react-native-elements';
import { auth, db } from '../config/firebase';
import { signOut, deleteUser } from 'firebase/auth';
import { doc, deleteDoc } from "firebase/firestore";
import BarCode from "../components/BarCode";
import StatusBar from '../components/StatusBar';
import TextInput from '../components/TextInput';
import Moment from 'moment';
import 'moment/locale/fr'

export default function ProfilScreen(props) {

    const user = auth.currentUser;

    const [modalVisible, setModalVisible] = useState(false);

    function SignOutUser() {
        signOut(auth).then(() => {
        }).catch((error) => {
            console.log(error)
        });
    }

    function DeleteUser() {
        deleteUser(user).then(() => {
            deleteDoc(doc(db, "users", user.uid));

        }).catch((error) => {
            console.log(error)
        });
    }

    return (
        <SafeAreaView style={styles.container}>
            <StatusBar />
            <Text style={styles.title}>
                Profil
            </Text>
            <View style={styles.viewInput}>
                <Text styles={styles.textDisabled}>Email</Text>
                <TextInput
                    name="email"
                    value={user.email}
                    editable={false}
                    selectTextOnFocus={false}
                    underlineColorAndroid='transparent'
                    color='#808080'
                />
            </View>
            <View style={styles.viewInput}>
                <Text styles={styles.textDisabled}>Date de création</Text>
                <TextInput
                    name="password"
                    value={Moment(user.metadata.creationTime).locale('fr').format('LL')}
                    editable={false}
                    selectTextOnFocus={false}
                    underlineColorAndroid='transparent'
                    color='#808080'
                />
            </View>
            <Button title="Se déconnecter" containerStyle={styles.buttonSignOut} onPress={SignOutUser} />
            <Button title="Supprimer mon compte" containerStyle={styles.buttonDeleteUser} buttonStyle={{ backgroundColor: '#DC143C' }} onPress={() => setModalVisible(true)} />
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    Alert.alert("Modal has been closed.");
                    setModalVisible(!modalVisible);
                }}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text style={styles.modalText}>Voulez-vous supprimer votre compte ?</Text>
                        <Button title="Oui" containerStyle={styles.buttonModal} buttonStyle={{ backgroundColor: '#DC143C' }} onPress={() => DeleteUser()} />
                        <Button title="Non" containerStyle={styles.buttonModal} onPress={() => setModalVisible(!modalVisible)} />
                    </View>
                </View>
            </Modal>
            <BarCode />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
    },

    title: {
        color: '#223e4b',
        fontSize: 20,
        marginBottom: 30,
        textAlign: 'center'
    },

    textDisabled: {
        fontSize: 30
    },

    viewInput: {
        paddingHorizontal: 32,
        marginBottom: 16,
        width: '100%'
    },

    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },

    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },

    buttonSignOut: {
        height: 44,
        width: 250,
        justifyContent: "center",
        marginTop: 10
    },

    buttonDeleteUser: {
        height: 44,
        width: 250,
        justifyContent: "center",
        marginTop: 20
    },

    buttonModal: {
        height: 44,
        width: 150,
        justifyContent: "center",
        marginTop: 10
    },

    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },

    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }

});