import React, { useState } from 'react';
import { Text, View, StyleSheet, SafeAreaView } from 'react-native';
import { auth, db } from '../config/firebase';
import { createUserWithEmailAndPassword, sendEmailVerification } from 'firebase/auth';
import { collection, doc, setDoc } from 'firebase/firestore';
import { Formik } from "formik";
import { Button } from 'react-native-elements';
import StatusBar from '../components/StatusBar';
import TextInput from '../components/TextInput';
import { LoginValidationSchema } from '../yup/LoginValidationSchema';

export default function RegisterScreen({ navigation }) {

    const [errorAlreadyUser, setErrorAlreadyUser] = useState('');
    const [successMessage, setSuccessMessage] = useState('');

    function RegisterUser(values, { resetForm }) {
        createUserWithEmailAndPassword(auth, values.email, values.password)
            .then((userCredential) => {
                sendEmailVerification(auth.currentUser)
                    .then(() => {
                        setErrorAlreadyUser("");
                        setSuccessMessage('Votre compte a bien été crée. Veuillez consulter le lien transmis par email pour valider votre compte.');
                        resetForm({ values: '' })
                        // Signed in 
                        const user = userCredential.user;
                        const usersRef = collection(db, "users");
                        return setDoc(doc(usersRef, user.uid), {
                            email: user.email,
                            createdAt: user.metadata.creationTime,
                            beers_scanned: [],
                            beers_favorite: [],
                        })
                    });
            })
            .catch((error) => {
                setErrorAlreadyUser('Un compte avec cet adresse email existe déja. Veuillez vous connecter ou réinitialiser votre mot de passe si vous l\'avez oublié.')
                console.log(error);
                const errorCode = error.code;
                const errorMessage = error.message;
                // ..
            })
    }

    return (
        <SafeAreaView style={styles.container} >
            <StatusBar />
            <Formik
                validationSchema={LoginValidationSchema}
                initialValues={{ email: "", password: "" }}
                onSubmit={(values, { resetForm }) => RegisterUser(values, { resetForm })}
            >
                {({
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    errors,
                    isValid,
                    values,
                    touched,
                }) => (
                    <>
                        <Text style={styles.title}>
                            Inscription
                        </Text>
                        <View style={styles.viewInput}>
                            <TextInput
                                name="email"
                                placeholder="Email"
                                keyboardType="email-address"
                                icon='mail'
                                autoCapitalize='none'
                                autoCompleteType='email'
                                keyboardAppearance='dark'
                                returnKeyType='next'
                                returnKeyLabel='next'
                                onChangeText={handleChange("email")}
                                onBlur={handleBlur("email")}
                                value={values.email}
                            />
                            {(errors.email && touched.email) ?
                                <Text style={{ fontSize: 10, color: "red" }}>{errors.email}</Text>
                                : null
                            }
                        </View>
                        <View style={styles.viewInput}>
                            <TextInput
                                name="password"
                                placeholder="Mot de passe"
                                icon='key'
                                secureTextEntry
                                autoCompleteType='password'
                                autoCapitalize='none'
                                keyboardAppearance='dark'
                                returnKeyType='go'
                                returnKeyLabel='go'
                                onChangeText={handleChange("password")}
                                onBlur={handleBlur("password")}
                                value={values.password}
                            />
                            {(errors.password && touched.password) ?
                                <Text style={{ fontSize: 10, color: "red" }}>
                                    {errors.password}
                                </Text>
                                : null
                            }
                        </View>
                        <Button
                            onPress={handleSubmit}
                            containerStyle={styles.button}
                            title="Inscription"
                            disabled={!isValid} />
                        {errorAlreadyUser ?
                            <Text style={{ fontSize: 10, color: "red" }}>{errorAlreadyUser}</Text>
                            : null
                        }
                        {successMessage ?
                            <Text style={{ fontSize: 10, color: "green", marginTop: 10 }}>{successMessage}</Text>
                            : null
                        }
                    </>
                )}
            </Formik>
        </SafeAreaView>

    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: "center",
        justifyContent: "center",
        flex: 1,

    },

    title: {
        color: '#223e4b',
        fontSize: 20,
        marginBottom: 16
    },

    viewInput: {
        paddingHorizontal: 32,
        marginBottom: 16,
        width: '100%'
    },

    button: {
        height: 44,
        width: 250,
        justifyContent: "center",
        marginTop: 10,
    },
});