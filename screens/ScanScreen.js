import React, { useState, useEffect } from "react";
import { Text, View, StyleSheet } from "react-native";
import { auth, db } from "../config/firebase";
import {
    collection,
    doc,
    getDocs,
    query,
    where,
    updateDoc,
    arrayUnion,
} from "firebase/firestore";
import { BarCodeScanner } from "expo-barcode-scanner";

export default function CameraScreen(props) {
    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);
    const [errorScan, seterrorScan] = useState("");

    const user = auth.currentUser;

    useEffect(() => {
        (async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === "granted");
        })();
    }, []);

    if (hasPermission === null) {
        return <Text>Requesting for camera permission</Text>;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }

    async function handleBarCodeScanned({ data }) {
        setScanned(true);

        const arrayBeer = {
            code: data,
            date: new Date(),
        };

        const q = query(collection(db, "beers"), where("code", "==", data));
        const querySnapshot = await getDocs(q);

        if (!querySnapshot.empty) {
            seterrorScan("");
            const userDoc = doc(db, "users", user.uid);
            await updateDoc(userDoc, {
                beers_scanned: arrayUnion(arrayBeer),
            });
            setScanned(false);
            props.navigation.navigate("Détails", {
                code: data,
            });
        } else {
            seterrorScan(
                "Code-barres non trouvé. Veuillez scanner un nouveau produit."
            );
        }
    }

    return (
        <View style={styles.container}>
            <Text style={styles.description}>Scan ton code-barres</Text>
            <BarCodeScanner
                onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
                style={[StyleSheet.absoluteFillObject, styles.barcodeContainer]}
            />
            {errorScan ? (
                <Text style={styles.errorMessage}>
                    {errorScan}
                </Text>
            ) : null}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        backgroundColor: "#000000",
    },

    barcodeContainer: {
        flex: 1,
        alignItems: "center",
    },

    description: {
        fontSize: 30,
        paddingTop: 50,
        textAlign: "center",
        width: "70%",
        color: "white",
    },

    errorMessage: {
        fontSize: 18,
        color: "red",
        marginTop: 440,
        width: "70%",
        textAlign: "center",
    }
});
