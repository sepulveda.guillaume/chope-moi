import React, { useState, useEffect } from 'react';
import { View, StyleSheet, SafeAreaView, FlatList } from 'react-native';
import { collection, getDocs, query, where } from "firebase/firestore";
import { db } from "../config/firebase";
import BeersDetails from "../components/BeersDetails";
import BarCode from "../components/BarCode";
import LoadingIndicator from '../components/LoadingIndicator';
import TextInput from '../components/TextInput';
import StatusBar from '../components/StatusBar';

export default function SearchScreen(props) {

    const [isLoading, setIsLoading] = useState(false);
    const [data, setData] = useState([]);
    const [searchItem, setSearchItem] = useState("");

    async function fetchData() {
        // Requête Firebase spécifique pour rechercher un document
        const q = query(collection(db, "beers"), where("product_name", '>=', searchItem), where("product_name", '<=', searchItem + '\uf8ff'));
        const querySnapshot = await getDocs(q);
        const arrayBeersInfos = [];

        querySnapshot.forEach((doc) => {
            arrayBeersInfos.push(doc.data());
        });

        setData(arrayBeersInfos);
        setIsLoading(false);
    }

    useEffect(() => {
        // On commence à rechercher si le mot utilisé dans la barre comportent au moins 3 caractères
        if (searchItem !== undefined && searchItem.length > 2) {
            setIsLoading(true);
            fetchData()
        } else {
            setData([]);
        }
    }, [searchItem])

    // On récupère la valeur du mot entré dans la barre de recherche à chaque changement de caractère
    const updateSearchItem = (searchItem) => {
        setSearchItem(searchItem);
    };

    function displayDetailsForBeer(code) {
        props.navigation.navigate("Détails", { code: code })
    }

    return (
        <SafeAreaView style={styles.container}>
            <StatusBar />
            <View style={styles.viewInput}>
                <TextInput
                    name="search"
                    placeholder="Recherche..."
                    icon='search'
                    onChangeText={updateSearchItem}
                    value={searchItem}
                />
            </View>
            {
                isLoading ? (
                    <LoadingIndicator />
                ) : (
                    <FlatList
                        data={data}
                        keyExtractor={({ code }, index) => code}
                        renderItem={({ item }) => <BeersDetails item={item} displayDetailsForBeer={displayDetailsForBeer} />}
                    />
                )
            }
            <BarCode />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 10
    },

    viewInput: {
        paddingHorizontal: 16,
        width: '100%',
        marginBottom: 10
    },

    loading: {
        flex: 1,
        justifyContent: "center",
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 10,
    },

    text_input: {
        marginLeft: 5,
    }
});
