import * as yup from "yup";

export const CommentsValidationSchema = yup.object().shape({
    comment: yup.string().required("Veuillez entrer un commentaire"),
    // note: yup
    //     .number()
    //     .min(0, ({ min }) => `Une note ne peut être inférieure à ${min}`)
    //     .max(5, ({ max }) => `Une note ne peut être supérieure à ${max}`)
    //     .required("Veuillez entrer une note"),
});