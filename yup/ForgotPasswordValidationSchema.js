import * as yup from 'yup';

export const ForgotPasswordValidationSchema = yup.object().shape({
    email: yup
        .string()
        .email("Entrer une adresse email valide")
        .required("Une adresse email est requise"),
});