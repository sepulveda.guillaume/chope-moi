import * as yup from 'yup';

// Schéma de validation du formulaire avec le composant yup
export const LoginValidationSchema = yup.object().shape({
    email: yup
        .string()
        .email("Entrer une adresse email valide")
        .required("Une adresse email est requise"),
    password: yup
        .string()
        .min(6, ({ min }) => `Le mot de passe doit comporter au moins ${min} caractères`)
        .required("Un mot de passe est requis"),
});